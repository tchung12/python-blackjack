===== BLACKJACK GAME =====

Objective: Implement a basic Blackjack game with a text user interface.
	Just use the basic rules of Blackjack, no double-down or insurance.
	Dealer must hit soft 17.

Implementation:
	1. Each Player's hand should be a list of the cards.
	2. Hint: Since we don't care about suits in the deck, there are just 4 of each type of card.
	3. To simplify this game, there is only the Dealer and Player1.
		Their hands, bets, and purses can be stored in main.

Additional features:
	1. Double-down and insurance options.
	2. Pay extra for blackjack 6:5
