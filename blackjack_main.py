from deck import *
from deal import *
from moves import *
from processes import *

# main game loop
def main():
	# start of game, *pursePlayer1* begins with 20 coins and *chooseToEnd* is 0 / False / No
	pursePlayer1 = 20
	chooseToEnd = 0

	# loop of turn intializing, checks to make sure *pursePlayer1* not empty and *chooseToEnd* is still 0 / False / No
	while pursePlayer1 > 0 and chooseToEnd == 0:
		dealerHand = []
		player1Hand = []
		deckList = createDeck()

		# asks for bet amount, then updates *pursePlayer1*
		print ('Player 1, you currently have ' + str(pursePlayer1) + ' coins in your purse. How many coins would you like to bet?')
		playerBet = input()
		pursePlayer1 -= int(playerBet)
		print ('You bet ' + str(playerBet) + ' coins and currently have ' + str(pursePlayer1) + ' remaining.')
		print ('')

		# shuffles deck then deals 2 carsds to each player, in alternating order
		deckList = shuffleDeck(deckList)
		dealInitialHands(player1Hand, dealerHand, deckList)
		
		# reveals first card from dealer hand, to the player 
		revealDealerHand(dealerHand)
		print ('')

		# player1 hand hit/end turn loop, returns total of hand
		totalPlayer1 = player1Decision(player1Hand, deckList)
		print ('')

		# if round is over from player1Decision - automatically lose, else - run dealer hand
		if totalPlayer1 > 21:
			roundEnd = True
			print ('Round is over')
			print ('')

		# dealer hand hit/end turn loop, returns total of dealer hand
		else:
			roundEnd = False
			totalDealer = sum(dealerHand)
			totalDealer = dealerDecision(dealerHand, deckList, totalDealer)
			print ('')
			
		# check scores for win/loss/tie	
		if roundEnd == True:
			print ('You lost the round and now have ' + str(pursePlayer1) + ' coins.')
		else:
			pursePlayer1 = scoreCheck(totalDealer, totalPlayer1, pursePlayer1, playerBet)

		print ('-----------------------------------------------------------------------------------------')
		print ('')

main()