

# revealDealerHand tells the player the upward facing card of the dealer
def revealDealerHand(dealerHand):
	if 1 in dealerHand:
		print ('The dealer is showing an Ace for his first card.')
	else:
		print ('The dealer is showing ' + str(dealerHand[0]) + ' for his first card.')

# checks total of dealer and player and determine who wins
def scoreCheck(totalDealer, totalPlayer1, pursePlayer1, playerBet):
	if (totalDealer > totalPlayer1 and totalDealer < 22) or (totalPlayer1 > 21):
		print ('You lost the round and now have ' + str(pursePlayer1) + ' coins.')

	elif (totalDealer < totalPlayer1) or (totalDealer > 21):
		newBalance = pursePlayer1 + int(playerBet)*2
		print ('You won the round and now have ' + str(newBalance) + ' coins.')
		pursePlayer1 = newBalance

	else:
		print ('The round was a tie and you do not gain or lose any coins.')
		pursePlayer1 = pursePlayer1 + int(playerBet)

	print ('')
	return pursePlayer1