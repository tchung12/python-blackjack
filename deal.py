

# deal deals cards for the player
def dealCard(whosHand, deckList):
	cardRemoved = takeCard(deckList)
	whosHand.append(cardRemoved)

# dealInitialHands gives player/dealer 2 cards at beginning of turn
def dealInitialHands(player1Hand, dealerHand, deckList):
	for handCards in range(0, 2):
		dealCard(dealerHand, deckList)
		dealCard(player1Hand, deckList)

# takeCard removes and returns the top card from the deck
def takeCard(deckList):
	cardRemoved = deckList.pop(0)
	return cardRemoved