import random

# createDeck creates a deck of cards
def createDeck():
	deckList = []
	for suit in range(1, 5):
		for cards in range(1, 11):
			deckList.append(cards)
		for royals in range(11, 14):
			deckList.append(10)
	return deckList

# shuffleDeck shuffles the deck of cards
def shuffleDeck(deckList):
	random.shuffle(deckList)
	return deckList