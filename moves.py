from deal import *

# player1 hand hit/end turn loop, returns total of hand
def player1Decision(player1Hand, deckList):	
	totalPlayer1 = sum(player1Hand)
	turnEnd = False
	while turnEnd == False:
		if totalPlayer1 > 21:
			print ('Your hand is', player1Hand)
			print ('You busted with a total greater than 21.')
			turnEnd = True
		else:
			print ('Your hand is', player1Hand)				
			print ('Would you like to hit (take another card)? y/n')
			userResponse = input()
			if userResponse == 'y':
				dealCard(player1Hand, deckList)
				totalPlayer1 = sum(player1Hand)
			else:
				turnEnd = True
	return totalPlayer1

# dealer hand hit/end turn loop, returns total of dealer hand
def dealerDecision(dealerHand, deckList, totalDealer):
	dealerTurnEnd = False
	foundAce = False
	totalDealer = checkAce(dealerHand, totalDealer, foundAce)
	while dealerTurnEnd == False:
		if totalDealer > 21:
			print ('The dealer hand is', dealerHand)
			print ('Dealer busted with a total greater than 21.')
			dealerTurnEnd = True
		elif totalDealer == 17 and foundAce == True:										# for soft 17, dealer hit
			print ('The dealer hand is', dealerHand, 'and will hit on soft 17.')
			totalDealer = revertAce(dealerHand, totalDealer, foundAce)
			dealCard(dealerHand, deckList)
			totalDealer = sum(dealerHand)
		elif totalDealer >= 17 and totalDealer <= 21:
			print ('The dealer hand is', dealerHand, 'and will end his/her turn.')		# non-soft 17, dealer ends turn
			dealerTurnEnd = True
		else:
			print ('The dealer hand is', dealerHand, 'and will hit.')
			dealCard(dealerHand, deckList)
			totalDealer = sum(dealerHand)
	return totalDealer

# if found Ace in dealer, add 10
def checkAce(whosHand, totalWhosHand, foundAce):
	if 1 in whosHand and foundAce == False:
		whosHand.remove(1)
		whosHand.append(11)
		foundAce = True
		totalWhosHand = sum(whosHand)
		return totalWhosHand
	else:
		return totalWhosHand

# if hit soft 17, revert add 10 so dealer can hit
def revertAce(whosHand, totalWhosHand, foundAce):
	whosHand.remove(11)
	whosHand.append(1)
	totalWhosHand = sum(whosHand)
	return totalWhosHand

