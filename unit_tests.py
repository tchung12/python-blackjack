import unittest
from deck import *
from moves import*
# from blackjack_main import *

class createDeckTest(unittest.TestCase):
	def testIsDeckBuilt(self):
		self.assertTrue(createDeck())
	def testHas52Cards(self):
		self.assertIs(len(createDeck()), int(52))

if __name__ == '__main__':
	unittest.main()